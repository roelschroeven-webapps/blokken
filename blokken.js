// vim: softtabstop=2 shiftwidth=2 cinoptions={1s,l1,g0,(1s,\:1s,i1s expandtab 
// vim: comments=sO\:*\ -,mO\:*\ \ ,exO\:*/,s1\:/*,mb\:*,ex\:*/,b\:///,b\:// 

var maxNrMatches = 1000;

var allWords = [];
var firstLetter = '';
var otherLetters = '';

var firstLetterField = document.querySelector('input#firstLetter');
var otherLettersField = document.querySelector('input#otherLetters');
var nrMatchesText = document.querySelector('#nrMatches');
var wordsList = document.querySelector('#words');

function downloadWords() 
{
  var request = new XMLHttpRequest();
  request.open('GET', '8.txt?ts=20230112T1745')
  request.responseType = 'text';
  request.onload = function() {
    var lines = request.response.split(/[\r\n]+/g);
    allWords = lines.filter(line => !line.startsWith('#'));
    update();
  };
  request.send();
}

function update()
{
  readInput();
  if (firstLetter != '' || otherLetters != '')
    words = findWords();
  else
    words = [];
  updateOutput(words);
}

function readInput()
{
  firstLetter = firstLetterField.value.charAt(0).toLowerCase();
  otherLetters = otherLettersField.value.toLowerCase();
}

function findWords()
{
  var otherLettersSorted = otherLetters.split('').sort();
  var pattern = '^.*' + otherLettersSorted.join('.*') + '.*$';
  var re = new RegExp(pattern);

  var matches = [];
  for (let word of allWords)
  {
    if (!word.startsWith(firstLetter))
      continue;
    sortedTail = word.slice(1).split('').sort().join('');
    if (sortedTail.match(re))
      matches.push(word);
    if (matches.length == maxNrMatches)
      break;
  }
  return matches;
}

function updateOutput(words)
{
  if (firstLetter == '' && otherLetters == '')
    nrMatchesText.textContent = '';
  else if (words.length === 0)
    nrMatchesText.textContent = 'geen overeenkomsten';
  else if (words.length >= maxNrMatches)
    nrMatchesText.textContent = '' + maxNrMatches + '+ overeenkomsten:';
  else if (words.length === 1)
    nrMatchesText.textContent = '1 overeenkomst:';
  else
    nrMatchesText.textContent = words.length + ' overeenkomsten:';

  while (wordsList.firstChild)
    wordsList.removeChild(wordsList.firstChild);
  for (let word of words)
  {
    var listItem = document.createElement('li');
    listItem.textContent = word;
    wordsList.appendChild(listItem);
  }
}

function init()
{
  downloadWords();
  firstLetterField.addEventListener('input', update);
  otherLettersField.addEventListener('input', update);
  otherLettersField.focus();
}

init();

